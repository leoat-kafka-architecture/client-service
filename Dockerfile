FROM amazoncorretto:11

WORKDIR /AppServer
ADD target/client-service.jar app.jar

ENTRYPOINT ["sh", "-c", "java ${JAVA_OPTS} -jar /AppServer/app.jar"]
