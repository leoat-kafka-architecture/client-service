# Client Service

## Introduction

This service is responsible to manage the client table in the database having the
CRUD operations for this entity. It makes part of a greater architecture composed
around Kafka, as such, the `client` table is also logged to Kafka through Kafka
Connect.

## Architecture Diagram

![Architecture Diagram](diagram.png)

