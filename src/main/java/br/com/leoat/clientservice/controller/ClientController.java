package br.com.leoat.clientservice.controller;

import br.com.leoat.clientservice.infrastructure.validation.OnUpdate;
import br.com.leoat.clientservice.infrastructure.validation.OnCreate;
import br.com.leoat.clientservice.model.Client;
import br.com.leoat.clientservice.model.dto.ClientDTO;
import br.com.leoat.clientservice.model.service.ClientService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/client")
public class ClientController {


    private final ClientService clientService;
    private final ModelMapper modelMapper;

    @GetMapping
    public List<ClientDTO> findAll() {
        return clientService.findAll()
                .stream().map(this::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ClientDTO findById(@PathVariable String id) {
        return toDTO(clientService.findById(id));
    }

    @PostMapping
    @Validated(OnCreate.class)
    @ResponseStatus(HttpStatus.CREATED)
    public ClientDTO save(@RequestBody @Valid ClientDTO dto) {
        return toDTO(clientService.save(toEntity(dto)));
    }

    @PutMapping("/{id}")
    @Validated(OnUpdate.class)
    public ClientDTO update(@PathVariable String id, @RequestBody @Valid ClientDTO dto) {
        return toDTO(clientService.update(id, toEntity(dto)));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable String id) {
        clientService.delete(id);
    }

    private ClientDTO toDTO(Client client) {
        return modelMapper.map(client, ClientDTO.class);
    }

    private Client toEntity(ClientDTO dto) {
        return modelMapper.map(dto, Client.class);
    }
}
