package br.com.leoat.clientservice.controller;

import br.com.leoat.clientservice.infrastructure.exception.ClientNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

@Slf4j
@RestControllerAdvice
public class ErrorHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ClientNotFoundException.class)
    public Map<String, String> clientNotFoundException(ClientNotFoundException e) {
      log.warn(e.getMessage());
      return Map.of("message", e.getMessage(), "status", HttpStatus.NOT_FOUND.toString());
    }

}
