package br.com.leoat.clientservice.infrastructure.exception;

public class ClientNotFoundException extends RuntimeException {
    public ClientNotFoundException(String id) {
        super(String.format("Client of id %s not found.", id));
    }
}
