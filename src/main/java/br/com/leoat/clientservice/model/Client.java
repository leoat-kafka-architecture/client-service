package br.com.leoat.clientservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "client")
public class Client {

    @Id
    private String id;

    private String name;

    private String surname;

    private String email;

    private Long balance;

    private boolean active;

    private LocalDateTime timestamp;

    @PrePersist
    public void onPrePersist() {
        this.timestamp = LocalDateTime.now();
    }

    @PreUpdate
    public void onPreUpdate() {
        this.timestamp = LocalDateTime.now();
    }

}
