package br.com.leoat.clientservice.model.dto;

import br.com.leoat.clientservice.infrastructure.validation.OnCreate;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ClientDTO {

    private String id;

    @NotBlank(groups = OnCreate.class)
    private String name;

    @NotBlank(groups = OnCreate.class)
    private String surname;

    @NotBlank(groups = OnCreate.class)
    private String email;

    @NotNull(groups = OnCreate.class)
    private Long balance;

    private boolean active;

}
