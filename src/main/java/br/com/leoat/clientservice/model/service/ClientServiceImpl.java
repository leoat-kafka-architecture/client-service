package br.com.leoat.clientservice.model.service;

import br.com.leoat.clientservice.infrastructure.repository.ClientRepository;
import br.com.leoat.clientservice.model.Client;
import br.com.leoat.clientservice.infrastructure.exception.ClientNotFoundException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    @Override
    public List<Client> findAll() {
        return clientRepository.findAll();
    }

    @Override
    public Client findById(String id) {
        return clientRepository.findById(id)
                .orElseThrow(() -> new ClientNotFoundException(id));
    }

    @Override
    public Client save(Client client) {
        client.setId(UUID.randomUUID().toString());
        return clientRepository.save(client);
    }

    @Override
    public Client update(String id, Client client) {
        Client currentClient = findById(id);

        if(StringUtils.isNotBlank(client.getName()))
            currentClient.setName(client.getName());

        if(StringUtils.isNotBlank(client.getSurname()))
            currentClient.setSurname(client.getSurname());

        if(StringUtils.isNotBlank(client.getEmail()))
            currentClient.setEmail(client.getEmail());

        if(Objects.nonNull(client.getBalance()))
            currentClient.setBalance(client.getBalance());

        if(client.isActive() != currentClient.isActive())
            currentClient.setActive(client.isActive());

        return clientRepository.save(currentClient);
    }

    @Override
    public void delete(String id) {
        clientRepository.deleteById(id);
    }
}
