package br.com.leoat.clientservice.model.service;

import br.com.leoat.clientservice.model.Client;

import java.util.List;

public interface ClientService {
    List<Client> findAll();
    Client findById(String id);
    Client save(Client client);
    Client update(String id, Client client);
    void delete(String id);
}
